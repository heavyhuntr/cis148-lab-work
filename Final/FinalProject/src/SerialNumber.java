/**
 * The SerialNumber class takes a software serial number in
 * the form of LLLLL-DDDD-LLLL where each L is a letter and
 * each D is a digit. The serial number has three groups of
 * characters, separated by hyphens. The class extracts the
 * three groups of characters and validates them.
 */

public class SerialNumber {
    // Declare class member variables/fields here. Refer
    // to the UML diagram provided in the project PDF
    String first = "";
    String second = "";
    String third = "";
    Boolean valid = false;

    /**
     * The constructor breaks a serial number into three
     * groups and each group is validated.
     * @param sn A serial number.
     */
    public SerialNumber(String sn) {
        //TODO: fully implement the constructor as described
        first = sn.substring(0,5);
        second = sn.substring(6,10);
        third = sn.substring(11);

        //System.out.println(first);
        //System.out.println(second);
        //System.out.println(third);
    }

    /**
     * The isValid method returns a boolean value indicating
     * whether the serial number is valid.
     *
     * @return true if the serial number is valid, false if not.
     */
    public boolean isValid() {//TODO: replace this return statement with what's required
        validate(first, second, third);
        return valid;
    }

    /**
     * The validate method sets the valid field to true if the serial
     * number is valid. Otherwise it sets valid to false. HINT: this
     * method will be used in the constructor
     */
    private void validate(String first, String second, String third) {//TODO: fully implement this method as described
        if(isFirstGroupValid(first) && isSecondGroupValid(second) && isThirdGroupValid(third)){
            valid = true;
        }
    }

    /**
     * The isFirstGroupValid method validates the first group of
     * characters.
     *
     * @return true if the group is valid, false if not.
     */
    private boolean isFirstGroupValid(String first) {//TODO: replace this return statement with what's required
        Boolean firstValid = false;
        if(Character.isLetter(first.charAt(0)) && Character.isLetter(first.charAt(1)) && Character.isLetter(first.charAt(2))
            && Character.isLetter(first.charAt(3)) && Character.isLetter(first.charAt(1))){
            firstValid = true;
        }
        return firstValid;
    }

    /**
     * The isSecondGroupValid method validates the second group of
     * characters.
     *
     * @return true if the group is valid, false if not.
     */
    private boolean isSecondGroupValid(String second) {//TODO: replace this return statement with what's required
        Boolean secondValid = false;
        if(Character.isDigit(second.charAt(0)) && Character.isDigit(second.charAt(1)) && Character.isDigit(second.charAt(2))
                && Character.isDigit(second.charAt(3))){
            secondValid = true;
        }
        return secondValid;
    }

    /**
     * The isThirdGroupValid method validates the third group of
     * characters.
     *
     * @return true if the group is valid, false if not.
     */
    private boolean isThirdGroupValid(String third) {//TODO: replace this return statement with what's required
        Boolean thirdValid = false;
        if(Character.isLetter(third.charAt(0)) && Character.isLetter(third.charAt(1)) && Character.isLetter(third.charAt(2))
                && Character.isLetter(third.charAt(3))){
            thirdValid = true;
        }
        return thirdValid;
    }
}
