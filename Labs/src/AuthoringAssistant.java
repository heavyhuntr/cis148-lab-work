import java.util.Scanner;

public class AuthoringAssistant {

   // Create all helper methods for the class here //
   public static int getNumOfNonWSCharacters(String str){
      int count = 0;

      for(int i = 0; i < str.length(); i++){
         if(!Character.isWhitespace(str.charAt(i))){
            //Character.isWhiteSpace(ch) will return true if ch is whitespace, false otherwise
            count++;
         }
      }
      return count;
   }

   public static int getNumOfWords(String str){
      int count = 0;


      return count;
   }

   public static int findText(String find, String inString){

      return 0;
   }

   public static String replaceExclamation(String str){
      return str.replace('!', '.');
   }

   public static String shortenSpace(String str){

      return "";
   }

   public static char printMenu(Scanner scnr) {
      char menuOp = ' ';

      System.out.println("\nMENU");
      System.out.println("c - Number of non-whitespace characters");
      System.out.println("w - Number of words");
      System.out.println("f - Find text");
      System.out.println("r - Replace all !\'s");
      System.out.println("s - Shorten spaces");
      System.out.println("q - Quit\n");

      while (menuOp != 'c' && menuOp != 'w' && menuOp != 'f' &&
                menuOp != 'r' && menuOp != 's' && menuOp != 'o' &&
                menuOp != 'q') {
         System.out.println( "Choose an option:");
         menuOp = scnr.nextLine().charAt(0);
      }

      return menuOp;
   }


   public static void main(String[] args) {
      Scanner scnr = new Scanner(System.in);
      String inputString = "";
      char menuChoice = ' ';
      String toFind = "";


      // Get string from user
      System.out.println("Enter a sample text:");
      inputString = scnr.nextLine();

      System.out.println("\nYou entered: " + inputString);

      // Execute menu command
      while (menuChoice != 'q') {
         menuChoice = printMenu(scnr);

         if (menuChoice == 'c') {
            System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(inputString));
            // menuChoice = ' ';
         }

         else if (menuChoice == 'w') {
            System.out.println("Number of words: " + getNumOfWords(inputString));
            // menuChoice = ' ';
         }

         else if (menuChoice == 'f') {
            System.out.println( "Enter a word or phrase to be found:");
            toFind = scnr.nextLine();
            System.out.println("\"" + toFind + "\"" + " instances: " + findText(toFind, inputString));
            // menuChoice = ' ';
         }

         else if (menuChoice == 'r') {
            inputString = replaceExclamation(inputString);
            System.out.println("Edited text: " + inputString);
            // menuChoice = ' ';
         }

         else if (menuChoice == 's') {
            inputString = shortenSpace(inputString);
            System.out.println("Edited text: " + inputString);
            // menuChoice = ' ';
         }
      }

      return;

   }
}
