import java.util.ArrayList;

public class ShoppingCart {
    //Declare object member variables here:
    private String customerName;
    private String currentDate;
    private ArrayList<ItemToPurchase> cartItems = new ArrayList<ItemToPurchase>();

    public ShoppingCart() {
        //Implement the default constructor using 'this' constructor call
        this("none", "19/10/2017");
    }

    public ShoppingCart(String name, String date) {
        customerName = name;
        currentDate = date;
    }

    public String getCustomerName()  {
        return customerName;
    }

    public String getDate() {
        return currentDate;
    }

    public void addItem(ItemToPurchase item) {
        //use ArrayList method here
        cartItems.add(item);
        return;
    }

    public void removeItem(String name) {
        boolean found = false;

        //Search the collection for item to be removed and remove if found
        int i = 0;
        while(!found && i < cartItems.size()) {
            if (cartItems.get(i).getName().equals(name)) {
                cartItems.remove(i);
                found = true;
            }
            i++;
        }
        if(!found) {
            System.out.println("Item not found in cart. Nothing removed.");
        }
        return;
    }

    public void modifyItem(ItemToPurchase item) {
        boolean found = false;

        //Search the collection for item to modify and modify if found
        int i = 0;
        while(!found && i < cartItems.size()) {
            if (cartItems.get(i).getName().equals(item.getName())) {
                ItemToPurchase foundItem = cartItems.get(i);
                if(!item.getDescription().equals("none")){
                    foundItem.setDescription(item.getDescription());
                }
                if(item.getPrice() != 0){
                    foundItem.setPrice(item.getPrice());
                }
                if(item.getQuantity() != 0){
                    foundItem.setQuantity(item.getQuantity());
                }
                found = true;
            }
            i++;
        }
        
        if(!found) {
            System.out.println("Item not found in cart. Nothing modified.");
        }
    }

    public int getNumItemsInCart() {
        int totalItems = 0;

        for(int i = 0; i < cartItems.size(); i++){
            totalItems += cartItems.get(i).getQuantity();
        }

        //Process all items in the cart to get the total number of items
        
        return totalItems;
    }

    public int getCostOfCart() {
        int totalCost = 0;

        //Process all items in the cart and calculate the total cart cost
        for(int i = 0; i < cartItems.size(); i++){
            ItemToPurchase item = cartItems.get(i);
            totalCost += (item.getPrice() * item.getQuantity());
        }

        return totalCost;
    }

    public void printTotal() {
        System.out.println("'s Shopping Cart - ");
        System.out.println("Number of Items: ");
        System.out.println("");

        //If shopping cart is not empty, print each item's cost
        //otherwise print "SHOPPING CART IS EMPTY"

        System.out.println("");
        System.out.println("Total: $");
    }

    public void printDescriptions() {
        System.out.println("'s Shopping Cart - ");
        System.out.println("");
        System.out.println("Item Descriptions");

        //If shopping cart is not empty, print each item's description
        //otherwise print "SHOPPING CART IS EMPTY"

    }
}
