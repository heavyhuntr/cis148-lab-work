public class ItemToPurchase {
   private String itemName;
   private int itemPrice;
   private int itemQuantity;
   private String itemDescription;
   
   public ItemToPurchase() {
      this("none", "none", 0 , 0);
      return;
   }

   public ItemToPurchase(String name, String description, int price, int quantity){
      itemName = name;
      itemDescription = description;
      itemPrice = price;
      itemQuantity = quantity;
      return;
   }
   
   public void setName(String name) {
      itemName = name;
      return;
   }

   public void setDescription(String description){
      itemDescription = description;
      return;
   }
   
   public void setPrice(int price) {
      itemPrice = price;
      return;
   }
   
   public void setQuantity(int quantity) {
      itemQuantity = quantity;
      return;
   }
   
   public String getName() {
      return itemName;
   }

   public String getDescription(){return itemDescription;}
   
   public int getPrice() {
      return itemPrice;
   }
   
   public int getQuantity() {
      return itemQuantity;
   }

   public void printItemPurchase() {
      System.out.println(itemQuantity + " " + itemName + " $" + itemPrice +  
                         " = $" + (itemPrice * itemQuantity));
   }

   public void printItemCost(){
      System.out.println(itemName + " " + itemQuantity + " @ $" + itemPrice + " = $" + (itemPrice * itemQuantity));
      return;
   }

   public void printItemDescription(){
      System.out.println(itemName + ": " + itemDescription);
      return;
   }
}
