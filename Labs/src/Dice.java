/**
 * Programmer: (Taylor Forest Bent)
 * Date: 2017-09-21
 * CIS148 Week 4 Lab
 *
 * This class will simulate a pair of dice with possible face values of 1 through 6
 */

import java.util.Random;

public class Dice {
    //Member variables(fields)
    private int die1; //stores value of first die
    private int die2; //stores value of second die
    private Random random;

    //Constructor
    public Dice() {
        random = new Random();
        roll();
    }

    //Member methods
    public void roll() {
        die1 = random.nextInt(6) + 1;
        die2 = random.nextInt(6) + 1;
    }

    public int getDie1()  {return die1;}
    public int getDie2()  {return die2;}
    public int getTotal() {return die1 + die2;}
}
