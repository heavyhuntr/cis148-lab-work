/**
 * Programmer: Meg Prescott
 * Date: 2017-09-28
 * Week 5 In-class Lab
 *
 * Checks for a valid domain name, which contains a valid TLD
 * and only a single period. Name can only contain letters, numbers
 * and the hyphen.
 */
import java.util.Scanner;
public class DomainValidator {
    private static String[] tlds = {"com","org","net","edu","biz","info","name"};

    private int numTLDS;

    public static void main(String[] args) {
        DomainValidator dv = new DomainValidator();
        dv.numTLDS = tlds.length;
        Scanner input = new Scanner(System.in);
        boolean isValidDomain = false;
        int periodCount = 0;
        int periodPosition = -1;
        String inString = "";
        String tld = "";

        while (inString.isEmpty()) {
            System.out.print("Enter a domain name to check: ");
            inString = input.nextLine();
        }

        //Look for correct number of periods
        for(int i=0; i < inString.length(); ++i) {
            if(inString.charAt(i) == '.') {
                periodCount++;
                periodPosition = i;
            }
        }

        if (periodCount == 1 &&
                periodPosition != 0 &&
                periodPosition != inString.length() - 1) {
            isValidDomain = true;
        }

        if (isValidDomain) {
            //greatbay.edu
            tld = inString.substring(periodPosition + 1);
            for(String sub : tlds){ //sub and store the ADDRESS of the string they refer to
                if (sub.equals(tld)){
                    System.out.println(inString + " is a valid domain name.");
                    return;
                }
            }
        }
        System.out.println(inString + " is not a valid domain name.");

        return;
    }
}


